﻿using System.Linq.Expressions;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class IncludeExtension
    {
        public static IQueryable<TEntity> Include2<TEntity>(this DbSet<TEntity> dbSet,
            params Expression<Func<TEntity, object>>[] includes) where TEntity : class
        {
            IQueryable<TEntity> query = null;
            foreach (var include in includes)
            {
                query = dbSet.Include(include);
            }
            return query == null ? dbSet : query;
        }
    }
}
