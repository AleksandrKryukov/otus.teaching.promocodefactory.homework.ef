﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext _context;
        private readonly DbSet<T> Data;

        public EfRepository(DataContext context)
        {
            _context = context;
            Data = _context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Data.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await Data.FirstOrDefaultAsync(x => x.Id == id);
        }
           
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await Data.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task CreateAsync(T item)
        {
            Data.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T item)
        {
            Data.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T item)
        {
            await _context.SaveChangesAsync();
        }

    }
}