﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CustomerPreference>().HasKey(k => new { k.CustomerId, k.PreferenceId });

            builder.Entity<CustomerPreference>()
                .HasOne(c => c.Customer)
                .WithMany(a => a.Preferences)
                .HasForeignKey(aa => aa.CustomerId);

            builder.Entity<CustomerPreference>()
                .HasOne(c => c.Preference)
                .WithMany()
                .HasForeignKey(aa => aa.PreferenceId);
        }

    }
}
