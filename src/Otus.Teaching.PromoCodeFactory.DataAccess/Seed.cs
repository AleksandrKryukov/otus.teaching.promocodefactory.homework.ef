﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class Seed
    {
        public static void SeedData(DataContext context)
        {
            context.Employees.AddRange(FakeDataFactory.Employees);
            context.Preferences.AddRange(FakeDataFactory.Preferences);
            context.Customers.AddRange(FakeDataFactory.Customers);
            context.SaveChanges();
        }
    }

}
